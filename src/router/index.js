import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'OutLoginLayout',
    component: () => import('../layouts/OutLoginLayout'),
    beforeEnter: (to, from, next) => {
      const _existeToken = localStorage.getItem('token')
      if (_existeToken) {
        next('/app')
      }
      next()
    },
    children: [
      {
        path: '/',
        name: 'login',
        component: () => import('../views/Login'),
        beforeEnter: (to, from, next) => {
          console.log('DESDE GUARD LOGIN')
          console.log(to, from)
          next()
        }
      }
    ]
  },
  {
    path: '/app',
    name: 'MainLayout',
    component: () => import('../layouts/MainLayout'),
    beforeEnter: (to, from, next) => {
      const _existeToken = localStorage.getItem('token')
      if (!_existeToken) {
        next('/')
      }
      console.log('DESDE BEFORE APP')
      console.log(_existeToken)
      next()
    },
    children: [
      {
        path: 'usuarios/:numeroUno/:numeroDos',
        name: 'usuarios',
        component: () => import('../views/Usuarios')
      },
      {
        path: 'menus',
        name: 'menus',
        component: () => import('../views/Menus')
      },
      {
        path: 'tareas',
        name: 'tareas',
        component: () => import('../views/Tareas')
      }
    ]
  },
  // {
  //   path: '/inicio',
  //   name: 'inicio',
  //   component: Index
  // },
  // {
  //   // /usuarios/crear
  //   path: '/usuarios/:numeroUno/:numeroDos',
  //   name: 'usuarios',
  //   component: () => import('../views/Usuarios')
  // }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router