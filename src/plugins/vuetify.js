import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';


Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#311B92', // #E53935
        secondary: '#42A5F5', // #FFCDD2
        accent: '#FFB74D', // #3F51B5
      },
    },
  },
});
