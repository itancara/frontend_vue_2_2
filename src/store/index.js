import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    contador: 855,
    usuario: {
      usuario: '',
      contrasena: ''
    },
    logueado: false
  },
  mutations: {
    incrementarStateContador (state, incremento) {
      console.log(incremento)
      state.contador += incremento
    },
    guardarUsuario (state, usuario) {
      state.usuario = usuario
    }
  },
  getters: {},
  actions: {}
})